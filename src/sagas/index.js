import { put, takeEvery, all } from 'redux-saga/effects';
import loadTodos from './load.todo';
import addTodo from './add.todos';

export default function* root() {
  yield all([loadTodos(), addTodo()]);
}
