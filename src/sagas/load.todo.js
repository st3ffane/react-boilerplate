import { put, takeEvery } from 'redux-saga/effects';
import waitALittle from './dumb';

export function* loadTodos() {
  console.log('loading todos....');
  yield waitALittle();
  console.log('loading todos....2');
  yield put({
    type: 'RENDER TODO',
    payload: {
      todos: ['do something', 'or another, I dont care...']
    }
  });
}

export default function* loadTodosSaga() {
  yield takeEvery('LOAD TODOS', loadTodos);
}
