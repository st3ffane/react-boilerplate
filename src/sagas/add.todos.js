import { put, takeEvery, call } from 'redux-saga/effects';
import waitALittle from './dumb';

export function* addTodo(todo) {
  yield waitALittle();
  yield put({
    type: 'ADD TODO',
    payload: {
      message: todo.payload.todostr
    }
  });
  // handle form reset here????
  yield put({
    type: 'SAVE OK'
  });
  // yield call(todo.payload.actions);
}

export default function* addTodoSaga() {
  yield takeEvery('SAVE TODO', addTodo);
}
