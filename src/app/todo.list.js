import React, { useState } from 'react';
import { connect } from 'react-redux';

import Grid from '@material-ui/core/Grid';

import TodoItem from './todo.item';
import Wait from './waiting';

function TodoList({ todo, dispatch }) {
  if (!todo.todos || todo.todos.length === 0) return <Wait />;
  return (
    <Grid container spacing={1}>
      {todo.todos.map((el, i) => {
        return (
          <Grid key={i} item xs={12}>
            <TodoItem todo={el} />
          </Grid>
        );
      })}
    </Grid>
  );
}

function mapStateToProps(state) {
  return {
    todo: state.todo
  };
}
export default connect(mapStateToProps)(TodoList);
