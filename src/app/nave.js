import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import { FormattedMessage } from 'react-intl';

export default function Nave() {
  return (
    <Grid container spacing={1}>
      <Grid item xs={1}>
        <Link to="/" component={RouterLink}>
          <FormattedMessage id="nav.home" />
        </Link>
      </Grid>
      <Grid item xs={1}>
        <Link to="/add" component={RouterLink}>
          <FormattedMessage id="nav.add" />
        </Link>
      </Grid>
    </Grid>
  );
}
