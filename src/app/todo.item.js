import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2)
  }
}));

function TodoItem({ todo }) {
  const classes = useStyles();
  return (
    <Paper className={classes.root}>
      <span>{todo}</span>
    </Paper>
  );
}

TodoItem.propTypes = {
  todo: PropTypes.string
};

export default TodoItem;
