import React, { useRef, useEffect } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';
// import TextField from '@material-ui/core/TextField';
import { TextField } from 'formik-material-ui';
import { injectIntl, FormattedMessage } from 'react-intl';

import MakeAsyncFunction from 'react-redux-promise-listener';
import { promiseListener } from 'store.js';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2),
    marginBottom: 10
  },
  search: {
    position: 'relative',
    top: 5
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    lineHeight: '42px'
  },
  rightIcon: {
    marginLeft: theme.spacing(1)
  }
}));

const AddTodoValidator = Yup.object().shape({
  todostr: Yup.string()
    .required()
    .min(5, 'Too short')
    .max(50, 'Too long!')
});

function Creator({ history, intl, dispatch }) {
  const classes = useStyles();

  return (
    <MakeAsyncFunction
      listener={promiseListener}
      start="SAVE TODO" // the type of action to dispatch when this function is called
      resolve="SAVE OK" // the type of action that will resolve the promise
      reject="REJECT_TODO" // the type of action that will reject the promise
    >
      {asyncFunc => (
        <Paper>
          <Grid container className={classes.root}>
            <Grid item xs={12}>
              <span className={classes.title}>
                <FormattedMessage id="hello" />
              </span>
            </Grid>

            <Grid item xs={12}>
              <Formik
                validationSchema={AddTodoValidator}
                initialValues={{ todostr: '' }}
                onSubmit={(values, { setSubmitting, resetForm }) => {
                  // saga are out of the flow, no way to check isSubmitting...
                  // dispatch({type:'SAVE TODO', payload:{message: values.todostr, actions:()=>{
                  return asyncFunc(values).then(() => {
                    resetForm();
                    setSubmitting(false);
                    // redirect to home
                    history.push('/');
                  });
                }}
              >
                {({ errors, touched, isSubmitting }) => {
                  return (
                    <Form>
                      <Grid
                        container
                        spacing={2}
                        direction="row"
                        justify="center"
                        alignItems="center"
                      >
                        <Grid item lg={10} md={9} xs={12}>
                          <Field
                            name="todostr"
                            autoCorrect="off"
                            autoFocus
                            autoCapitalize="none"
                            autoComplete="off"
                            component={TextField}
                            fullWidth
                            label={intl.formatMessage({
                              id: 'add.input.label'
                            })}
                          />
                        </Grid>
                        <Grid item lg={2} md={3} xs={12}>
                          <Button
                            variant="contained"
                            fullWidth
                            color="primary"
                            type="submit"
                            disabled={isSubmitting ? true : null}
                            className={classes.search}
                          >
                            <FormattedMessage id="add.action" />
                            <Icon className={classes.rightIcon}>send</Icon>
                          </Button>
                        </Grid>
                      </Grid>
                    </Form>
                  );
                }}
              </Formik>
            </Grid>
          </Grid>
        </Paper>
      )}
    </MakeAsyncFunction>
  );
}

/*Creator.propTypes = {
  title: PropTypes.string.isRequired
};*/

export default connect()(withRouter(injectIntl(Creator)));
