import React, { useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import { connect } from 'react-redux';

import TodoCreator from './app/todo.creator';
import TodoList from './app/todo.list';
import Nav from './app/nave';

import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import { Route } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  page: {
    padding: theme.spacing(3, 2)
  }
}));

function App({ todo, dispatch }) {
  const classes = useStyles();

  useEffect(() => {
    dispatch({ type: 'LOAD TODOS' });
  }, []);
  return (
    <Container maxWidth="lg" className={classes.page}>
      <Nav />
      <Route path="/" exact component={TodoList} />
      <Route path="/add" exact component={TodoCreator} />
    </Container>
  );
}

function mapStateToProps(state) {
  return {
    todo: state.todo
  };
}
export default connect(mapStateToProps)(App);
