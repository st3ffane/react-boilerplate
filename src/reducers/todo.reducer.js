import { handleActions } from 'redux-actions';

export default handleActions(
  {
    Hello: (state, action) => {
      return {
        ...state,
        message: 'hello'
      };
    },
    'ADD TODO': (state, action) => {
      console.log('Add todo: ', action);
      state.todos.push(action.payload.message);
      return {
        ...state
      };
    },
    'RENDER TODO': (state, action) => {
      state.todos.push(...action.payload.todos);
      return { ...state };
    }
  },
  {
    message: 'Welcome to my fabulous application!',
    todos: []
  }
);
