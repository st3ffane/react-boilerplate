import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import createReduxPromiseListener from 'redux-promise-listener';

// ours reducers
import reducers from './reducers';
//ours sagas
import sagas from './sagas';
// create sagas
const sagamddwr = createSagaMiddleware();
const reduxPromiseListener = createReduxPromiseListener();

const store = createStore(
  reducers,
  applyMiddleware(sagamddwr, reduxPromiseListener.middleware)
);
sagamddwr.run(sagas);

export const promiseListener = reduxPromiseListener;
export default store;
